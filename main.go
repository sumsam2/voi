
package main

import (
	"fmt"
	"app/sm"	
	"encoding/json"	
	"io/ioutil"	
	"strconv"
)


// The vehicle struct. 
// It holds a pointer to its statemachine. 
// Instance of Vechicle should be created Using NewVechicle()
type Vehicle struct {
	Id int
    Curr string
    
}

func (v *Vehicle) GetId() int{
	return v.Id
}

func (v *Vehicle) GetCurrent() string{
	return v.Curr
}

func (v *Vehicle) SetCurrent(curr string) {
	v.Curr = curr
}


// Function creates the instance of the vehicle
func NewVechicle(Id int, Curr string) *Vehicle {
	
    v := &Vehicle{
        Id: Id,
        Curr: Curr,
    }
   
    return v
}

func beforeEvent(event *sm.Event,v sm.IVehicle) {
    fmt.Println("BeforeEvent: Vehicle " + strconv.Itoa(v.GetId())  + " Trying: " + event.Src + " --> " + event.Dst + " {User Role: " + event.UserRole + "}")
}


func afterEvent(event *sm.Event,v sm.IVehicle) {
    fmt.Println("AfterEvent: Vehicle " + strconv.Itoa(v.GetId()) + " state changed: " + event.Src + " --> " + event.Dst + " {User Role: " + event.UserRole + "}")
}


func main() {

	// Reading te events.json file and mapping it on the struct sm.Events
	file_events, _ := ioutil.ReadFile("events.json")
	data := sm.Events{}
	_ = json.Unmarshal([]byte(file_events), &data)
	
	// Initializing the vechicle.	
	vehicle := NewVechicle(1,"Ready")

	// Initializing the State Machine.	
   	statemachine := sm.NewStateMachine(data.Events,
    	sm.Callbacks{
            sm.CallbackBeforeEvent: func(event *sm.Event,v sm.IVehicle) { beforeEvent(event,v) },
            sm.CallbackAfterEvent: func(event *sm.Event,v sm.IVehicle) { afterEvent(event,v) },
        },
    )

	// Running some tests events. More test events can be defined in the test.json file.
	// The below code will try to trigger the list of events defined in test.json file. 
	file_tests, _ := ioutil.ReadFile("test.json")	
	test := sm.Events{}
	_ = json.Unmarshal([]byte(file_tests), &test)

	fmt.Println("Vehicle Initital State: " +  vehicle.Curr)	

	fmt.Println("=============================")
	for _, e := range test.Events{

		
		err := statemachine.Event(&sm.Event{vehicle.Curr,e.Dst,e.UserRole},vehicle)	
		if err !=nil {			
			fmt.Println(err)
		}
		fmt.Println("Vehicle Current State: " +  vehicle.Curr)
		fmt.Println("=============================")
	}	
}