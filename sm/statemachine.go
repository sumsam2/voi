package sm

import (	
	"strings"	
)


// Event Struct that holds the Src, Dst and User role(Event Initiator)
type Event struct {	
	Src string `json:"src"`
	Dst string `json:"dst"`
	UserRole string `json:"user_role"`
}

// Slice of Events

type Events struct {	
	Events []Event `json:"events"`
}

// Callback type. This is used to hold a reference to a user defined callback function.
type Callback func(*Event,IVehicle)


// Slice of callback functions
type Callbacks map[string]Callback


// interface that the vehicle should implement. 
type IVehicle interface {
	GetId() int
	GetCurrent() string
	SetCurrent(curr string)
}


// State machine. It maintains the states and transitions. 
// it should be created with NewStateMachine() function.
type StateMachine struct {
		
	// The mao holds all the possible states.
	states map[string]bool

	// The slice holds all the user defined transitions of the state machine.
	Transitions []Event 

	// The map holds all the possible events.
	Events map[Event]bool

	// Type holds the callbacks. 
	callbacks Callbacks

	// The map holds all the user defined user roles. 
	availableUserRoles map[string]bool
	 

}

// Creates the instance of the statemachine. Initializes all the required variables. 
// events can be nil. if nil the default transitions are loaded into the state machine
// callbacks can be nil. If nil statemachine.Event doesnot fire the callback functions. 
func NewStateMachine(events []Event, callbacks Callbacks) *StateMachine {

	if events == nil {
		events = defaultEvents()
		
	}

	statemachine := &StateMachine {		

		states: make(map[string]bool),

		Transitions: events,

		Events: make(map[Event]bool),

		callbacks: callbacks,

		availableUserRoles: make(map[string]bool),

	}

	for _, t := range statemachine.Transitions {		
		statemachine.Events[Event{t.Src,t.Dst,t.UserRole}] = true
		statemachine.states[t.Src] = true
		statemachine.states[t.Dst] = true
		statemachine.availableUserRoles[t.UserRole] = true
		
	}

	return statemachine
}


// Set of constants used to define the type of callback functions. 
const (
	CallbackBeforeEvent string = "CallbackBeforeEvent"	
	CallbackAfterEvent string = "CallbackAfterEvent"	
)


// Function peforms the specified event. Moves the statemachine from source to destination state if possioble.
// otherwise returns a descriptive error. 
func (statemachine *StateMachine) Event(event *Event, v IVehicle) error {


	statemachine.beforeEvent(event,v)


	if strings.Compare(event.UserRole, Admin)==0 {
		
		if _ , ok := statemachine.states[v.GetCurrent()]; !ok {
			return InvalidSourceError{v.GetCurrent()}
		}

		if _ , ok := statemachine.states[event.Dst]; !ok {
			return InvalidDestinationError{event.Dst}
		}

		if strings.Compare(event.Dst, v.GetCurrent())==0 {
			return SameSourceDestinationError{event.Dst}
		}

		v.SetCurrent(event.Dst)		
		statemachine.afterEvent(event,v)
		return nil


	} else {

		if _ , ok := statemachine.availableUserRoles[event.UserRole]; !ok {
			return InvalidUserError{event.UserRole}
		}

		if _ , ok := statemachine.states[v.GetCurrent()]; !ok {
			return InvalidSourceError{v.GetCurrent()}
		}

		if _ , ok := statemachine.states[event.Dst]; !ok {
			return InvalidDestinationError{event.Dst}
		}

		if strings.Compare(event.Dst, v.GetCurrent())==0 {
			return SameSourceDestinationError{event.Dst}
		}

		if _, ok := statemachine.Events[*event]; !ok {
			return PermissionError{v.GetCurrent(),event.Dst,event.UserRole}	
		}
		
		v.SetCurrent(event.Dst)
		statemachine.afterEvent(event,v)
		return nil
		
	}	
}

// fires the CallbackBeforeEvent callback if available
func (statemachine *StateMachine) beforeEvent(event *Event, v IVehicle)  {

	if fn, ok := statemachine.callbacks[CallbackBeforeEvent];  ok {
		fn(event,v)
	}
}

// fires the CallbackAfterEvent callback if available
func (statemachine *StateMachine) afterEvent(event *Event, v IVehicle)  {

	if fn, ok := statemachine.callbacks[CallbackAfterEvent]; ok {
		fn(event,v)
	}
}

// Set of Constants defined for the default available states of the state machine
const (
    Ready string = "Ready"
    Battery_low string = "Battery_low"
    Bounty string = "Bounty"
    Riding string = "Riding" 
    Collected string = "Collected"
    Dropped string = "Dropped"
    Service_mode string = "Service_mode"
    Terminated string = "Terminated"
    Unknown string = "Unknown"
)


// Set of default user roles (Event Initiator).
const (
	End_user string = "End_user"
    Hunter string = "Hunter"
    Admin string = "Admin"
    Automatic string = "Automatic"
)


// Function returns the defaul events. It is called when user code does not provide user defined events. 
func defaultEvents() []Event {
	events := [] Event {
			{Src: Dropped, 		Dst: Ready, 		UserRole: Hunter},
			{Src: Riding, 		Dst: Ready, 		UserRole: End_user},
			{Src: Riding, 		Dst: Ready, 		UserRole: Hunter},
			{Src: Ready, 		Dst: Riding, 		UserRole: End_user},
			{Src: Ready, 		Dst: Riding, 		UserRole: Hunter},
			{Src: Riding, 		Dst: Battery_low, 	UserRole: Automatic},
			{Src: Ready, 		Dst: Bounty, 		UserRole: Automatic},
			{Src: Battery_low, 	Dst: Bounty, 		UserRole: Automatic},
			{Src: Bounty, 		Dst: Collected, 	UserRole: Hunter},
			{Src: Collected, 	Dst: Dropped, 		UserRole: Hunter},
			{Src: Ready, 		Dst: Unknown, 		UserRole: Automatic},
		}
	return events
}

