package sm



// Statemachine.Event returns this error to the user code when destination state of the event is an invalid state
type InvalidDestinationError struct {
	Dst string	
}

func (e InvalidDestinationError) Error() string {
	return "Error: Destination State " + e.Dst + " does not exist."
}

// Statemachine.Event returns this error to the user code when Source state of the event is an invalid state
// This error should never happen if the vehicle state is being changed by the state machine. Because
// state machine will only move the vehicle to a valid state. 
type InvalidSourceError struct {
	Src string	
}

func (e InvalidSourceError) Error() string {
	return "Error: Source State " + e.Src + " does not exist."
}


// Statemachine.Event returns this error to the user code when destination state is equal to current state(src state).
type SameSourceDestinationError struct {
	name string

}

func (e SameSourceDestinationError) Error() string {
	return "Error: Already in state: " + e.name
}


// Statemachine.Event returns this error to the user code when the event is initiated by an invalid user role. 
type InvalidUserError struct {
	user string
}

func (e InvalidUserError) Error() string {
	return "Error: User Role " + e.user + " does not exist."
}


// Statemachine.Event returns this error to the user code when the user role does not have permissions to do the transition.
type PermissionError struct {
	Src string
	Dst string
	UserRole string
}

func (e PermissionError) Error() string {
	return "Error: User Role " + e.UserRole + " does not have permission to transition from " + e.Src + " to " + e.Dst
}



