# Solution for Handling State Transitions. 

The solution is implemented in the form of a _state machine_. State machine handles and validates all state transitions of the vehicle. The solution does not provide any sophisticated unit tests. However, it does provide a simple way of executing some test events on the state machine. The solution contains custom errors as well. State machine and custom errors are part of the package `sm`, short for state machine. 

## Usage

Sample usage is provided in the `main.go` file. 

Any abstract `Vehicle` Struct should implement the `sm.IVehilce` interface

```Go
type IVehicle interface {
	GetId() int
	GetCurrent() string
	SetCurrent(curr string)
}
```

### Instantiate State Machine
Instantiate state machine using `sm.NewStateMachine` fuction. It takes two arguments `[]sm.Event` and `sm.Callbacks`.

`[]sm.Event`: It is a list of possible events (or transitions) that the state machine can do. `sm.Event` is a type defined in the package `sm` that holds a generic event. It contains 3 fields. `Src, Dst, UserRole`. The Source state, Destination state and the user role that has the permission to do this particular transition depending on the business rules. In the sample usage, these events are loaded from a json file `events.json`. Define all the state transitions and associated user role permission in this file. 

`sm.Callbacks`: These are the functions that state machine calls in response to certain events. Only two types of callbacks are implemented: `CallbackBeforeEvent` and `CallbackAfterEvent`. User defined functions are called before and after a transition of state. For sample usage see `main.go`

Both of these arguments can be `nil`. In case of `nil` event list, a default set of events are loaded into the state machine. And for `nil` callbacks, user code doesn't receive callbacks from statemachine. 

### Triggering an event.

To initiate a state change call `StateMachine.Event(&event,IVehicle)` method. Input the event that you want to trigger and the instance of the vehicle. It will return nil if the statte transtion is executed successfully otherwise a descriptive custom error.

In the sample usage `main.go` test events are loaded from `test.json` file. Define any state transition that you want to trigger in that file and `main.go` will execute all those transition on the state machine. In the `test.json` file you only need to add the destination state and user role that is initiating the transition. State machine will validate the transition depending on the vehicle's current state. 

## Execution

For execution `Dockerfile` is provided. It requires docker to be installed on the machine. If docker is not installed, ignore `Dockerfile` and just comple the `go` files. For docker users execute these commands in the directory of the `Dockerfile`

```cmd
sudo docker build -t <any_tag> .
sudo docker run <any_tag>
```

This will install and run the `go` project files. Test events defined in the `test.json` file will be executed for a sample vehicle

## Notes
The current implementation of the state machine does not support adding a new transition into the state machine events map. The events/transitions provided on creating the instance of the state machine are kept in the map of the state machine throughout its lifetime. 

Sample usage provided creates an independant state machine. Which can be used to validate transitions of any abstract vechicle that conforms to the interface `sm.IVehicle`. The previous implementation (git commit: `1f594be05045cd61d4edffc8497dc223c7f35cba`) provides a sample usage with each vehicle having its own state machine, which can be useful when some vehicle types need a different set of state transitions. 

All the automatic events need to be detected by the client code and a call to `statemachine.Event` function should be made. State machine does not perform any timeout events for automatic transitions since it needs to be stateless.





